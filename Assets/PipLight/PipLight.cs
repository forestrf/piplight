﻿using UnityEngine;
using UnityEngine.Rendering;

[ExecuteInEditMode]
public class PipLight : MonoBehaviour
{
	public float radius = 16.0f;
	public Color color = Color.white;
	public float intensity = 1.0f;
	[Range(0, 1)]
	public float lightOffset = 0;
	public LightShadows shadowType = LightShadows.Hard;
	public ShadowTextureSize shadowMapResolution = ShadowTextureSize.x256;
	public ShadowRefreshMode shadowRefreshMode = ShadowRefreshMode.EveryFrame;
	public float levelOfDetailAggression = 1.0f;
	[Range(0, 1)]
	public float shadowBias = 0.03f;
	[Range(0, 1)]
	public float shadowStrength = 1;
	public LayerMask cullingMask = -1;
	public Cubemap cookie;

	[System.NonSerialized]
	public bool renderedThisFrame;

	[System.NonSerialized]
	public bool UpdateNextFrame = true;
	
	RenderTexture shadowMap;
	MaterialPropertyBlock propertyBlock;
	ShadowTextureSize shadowResolutionCurrent = ShadowTextureSize.x256;
	Camera shadowMapCamera;

	static int prop_Pip_LightPositionRange;
	static int prop_LightPositionRange;
	static int prop_ShadowMapTexture;
	static int prop_LightColor;
	static int prop_LightPos;
	static int prop_ShadowData;
	static int prop_LightTexture0;
	static int prop_LightMatrix0;
	static int prop_LightAsQuad;
	static int prop_LightOffset; 

	static Shader depthShader;

	[SerializeField]
	void OnEnable ()
	{	
		PipLightSystem.Instance.Add (this);
		UpdateNextFrame = true;
	}

	[SerializeField]
	void OnDisable ()
	{
		PipLightSystem.Instance.Remove (this);
		if (shadowMap) {
			DestroyImmediate (shadowMap);
			shadowMap = null;
		}
	}

	[SerializeField]
	void OnDrawGizmosSelected ()
	{
		Gizmos.DrawWireSphere (transform.position, radius);
	}

	[SerializeField]
	void OnDrawGizmos ()
	{
		Gizmos.color = color;
		Gizmos.DrawIcon(transform.position, "PointLight Gizmo", true);
	}

	void CheckCamera ()
	{
		if (!shadowMapCamera) {
			shadowMapCamera = GetComponent<Camera> ();
			if (!shadowMapCamera) {
				shadowMapCamera = gameObject.AddComponent<Camera> ();
			}
			shadowMapCamera.hideFlags = HideFlags.NotEditable | HideFlags.HideInInspector | HideFlags.HideInHierarchy;
			shadowMapCamera.clearFlags = CameraClearFlags.SolidColor;
			shadowMapCamera.backgroundColor = Color.white;
			shadowMapCamera.useOcclusionCulling = false;
			shadowMapCamera.hdr = true;
			shadowMapCamera.enabled = false;
			shadowMapCamera.nearClipPlane = 0.01f;
			shadowMapCamera.renderingPath = RenderingPath.VertexLit;
			if (depthShader == null) {
				depthShader = Resources.Load<Shader> ("PipLightDepth");
			}
			shadowMapCamera.SetReplacementShader(depthShader, "RenderType");
		}
		shadowMapCamera.cullingMask = cullingMask;
	}

	void CheckTexture ()
	{
		if (!shadowMap) {
			shadowMap = new RenderTexture ((int)shadowResolutionCurrent, (int)shadowResolutionCurrent, 0, RenderTextureFormat.RHalf, RenderTextureReadWrite.Linear);
			shadowMap.hideFlags = HideFlags.DontSave;
			shadowMap.isCubemap = true;
			shadowMap.useMipMap = false;
			shadowMap.generateMips = false;
		} else if (shadowMap.height != (int)shadowResolutionCurrent) {
			shadowMap.Release ();
			shadowMap.width = (int)shadowResolutionCurrent;
			shadowMap.height = (int)shadowResolutionCurrent;
			shadowMap.Create ();
		}
	}

	/// Called before the PipLightRenderer will render
	public void BeforeRender ()
	{
		renderedThisFrame = false;
	}

	public MaterialPropertyBlock GetMaterialPropertyBlock ()
	{
		if (propertyBlock == null) {
			propertyBlock = new MaterialPropertyBlock ();
		} else {
			propertyBlock.Clear ();
		}
		if (shadowType != LightShadows.None) {
			propertyBlock.SetVector (prop_ShadowData, new Vector4 { x = 1 - shadowStrength });
			propertyBlock.SetTexture (prop_ShadowMapTexture, shadowMap);
		}

		if (cookie != null) {
			propertyBlock.SetTexture (prop_LightTexture0, cookie);
			propertyBlock.SetMatrix (prop_LightMatrix0, transform.worldToLocalMatrix);
		}
		Vector4 _LightPos = transform.position;
		_LightPos.w = 1.0f / radius;
		propertyBlock.SetVector (prop_LightPositionRange, _LightPos);
		_LightPos.w /= radius;
		propertyBlock.SetVector (prop_LightPos, _LightPos);
		propertyBlock.SetVector (prop_LightColor, color.linear * intensity);
		propertyBlock.SetFloat (prop_LightAsQuad, 0f);
		propertyBlock.SetFloat(prop_LightOffset, lightOffset);
		return propertyBlock;
	}

	public void UpdateLOD (Camera cam)
	{
		Vector3 v1 = cam.transform.position;
		Vector3 v2 = transform.position;
		float distance = Vector3.Distance (v1, v2) * levelOfDetailAggression;
		float qualityMultiplier;
		if (distance <= radius) {
			qualityMultiplier = 1.0f;
		} else {
			qualityMultiplier = radius / distance;
		}
		int qualityNew = Mathf.ClosestPowerOfTwo ((int)((int)shadowMapResolution * qualityMultiplier));
		qualityNew = Mathf.Max (64, qualityNew);
		if (qualityNew != (int)shadowResolutionCurrent) {
			shadowResolutionCurrent = (ShadowTextureSize)qualityNew;
			UpdateNextFrame = true;
		}
		CheckTexture ();
	}

	public void UpdateShadowMap ()
	{
		if (!renderedThisFrame && shadowType != LightShadows.None) {
			CheckCamera ();
			if (UpdateNextFrame || shadowRefreshMode == ShadowRefreshMode.EveryFrame) {
				Vector4 positionRange = transform.position;
				positionRange.w = 1.0f / radius;
				Shader.SetGlobalVector (prop_Pip_LightPositionRange, positionRange);
				shadowMapCamera.farClipPlane = radius;
				shadowMapCamera.RenderToCubemap (shadowMap);
				UpdateNextFrame = false;
				renderedThisFrame = true;
			}
		}
	}

	float prevRadius = 0;
	Vector3 lastPosition;
	Matrix4x4 matrix;
	public void WriteToCommandBuffer(CommandBuffer cameraBuffer, Mesh lightSphereMesh, Material material) {
		if (prevRadius != radius || !gameObject.isStatic && transform.position != lastPosition) {
			lastPosition = transform.position;
			prevRadius = radius;
			matrix = Matrix4x4.TRS(transform.position, Quaternion.identity, Vector3.one * radius * 2f);
		}
		cameraBuffer.DrawMesh(
			lightSphereMesh,
			matrix,
			material,
			0,
			0,
			GetMaterialPropertyBlock()
		);
	}

	public static void CheckKeywords ()
	{
		prop_Pip_LightPositionRange = Shader.PropertyToID ("Pip_LightPositionRange");
		prop_LightPositionRange = Shader.PropertyToID ("_LightPositionRange");	
		prop_ShadowMapTexture = Shader.PropertyToID ("_ShadowMapTexture");
		prop_LightColor = Shader.PropertyToID ("_LightColor");
		prop_LightPos = Shader.PropertyToID ("_LightPos");
		prop_ShadowData = Shader.PropertyToID ("_LightShadowData");
		prop_LightTexture0 = Shader.PropertyToID ("_LightTexture0");
		prop_LightMatrix0 = Shader.PropertyToID ("_LightMatrix0");
		prop_LightAsQuad = Shader.PropertyToID ("_LightAsQuad");
		prop_LightOffset = Shader.PropertyToID ("_LightOffset");
	}

	public enum ShadowTextureSize
	{
		x8 = 8,
		x16 = 16,
		x32 = 32,
		x64 = 64,
		x128 = 128,
		x256 = 256,
		x512 = 512,
		x1024 = 1024,
		x2048 = 2048,
		x4096 = 4096,
		x8192 = 8192
	}

	public enum ShadowRefreshMode
	{
		EveryFrame,
		Manual
	}
}