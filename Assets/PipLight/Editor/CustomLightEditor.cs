using UnityEditor;
using UnityEngine;

[CustomEditor (typeof(PipLight))]
[CanEditMultipleObjects]
public class CustomLightEditor : Editor
{
	SerializedProperty radius,
		color, intensity, shadowType, shadowStrength,
		shadowMapResolution, shadowBias, shadowRefreshMode,
		levelOfDetailAggression, cookie, cullingMask,
		lightOffset;

	void OnEnable() {
		radius = serializedObject.FindProperty("radius");
		color = serializedObject.FindProperty("color");
		intensity = serializedObject.FindProperty("intensity");
		shadowType = serializedObject.FindProperty("shadowType");
		shadowStrength = serializedObject.FindProperty("shadowStrength");
		shadowMapResolution = serializedObject.FindProperty("shadowMapResolution");
		shadowBias = serializedObject.FindProperty("shadowBias");
		shadowRefreshMode = serializedObject.FindProperty("shadowRefreshMode");
		levelOfDetailAggression = serializedObject.FindProperty("levelOfDetailAggression");
		cookie = serializedObject.FindProperty("cookie");
		cullingMask = serializedObject.FindProperty("cullingMask");
		lightOffset = serializedObject.FindProperty("lightOffset");
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();
		EditorGUILayout.PropertyField(radius, new GUIContent("Range"));
		EditorGUILayout.PropertyField(color, new GUIContent("Color"));
		EditorGUILayout.PropertyField(intensity, new GUIContent("Intensity"));
		EditorGUILayout.PropertyField(lightOffset, new GUIContent("lightOffset", "Used to hide shadow artifacts"));
		EditorGUILayout.PropertyField(shadowType, new GUIContent("Shadow Type"));
		if ((target as PipLight).shadowType != LightShadows.None) {
			EditorGUI.indentLevel++;
			EditorGUILayout.PropertyField(shadowStrength, new GUIContent("Shadow Strength"));
			EditorGUILayout.PropertyField(shadowMapResolution, new GUIContent("Shadow Resolution"));
			EditorGUILayout.PropertyField(shadowBias, new GUIContent("Shadow Bias"));
			EditorGUILayout.PropertyField(shadowRefreshMode, new GUIContent("Shadow Refresh Mode"));
			EditorGUILayout.PropertyField(levelOfDetailAggression, new GUIContent("Level of detail aggressiveness"));
			EditorGUI.indentLevel--;
		}
		EditorGUILayout.PropertyField(cookie, new GUIContent("Cookie"));
		EditorGUILayout.PropertyField(cullingMask, new GUIContent("Shadow casting layers"));
		EditorGUILayout.Space ();
		if (GUILayout.Button ("Refresh all shadows")) {
			PipLightSystem.Instance.RefreshAll ();
		}
		Object.FindObjectOfType<PipLightRenderer> ().ForceRefresh ();
		serializedObject.ApplyModifiedProperties();
	}

	[MenuItem ("GameObject/Light/Pip Light")]
	static void CreatePipLight (MenuCommand menuCommand)
	{
		GameObject go = new GameObject ("Pip Light");
		go.AddComponent<PipLight> ();
		GameObjectUtility.SetParentAndAlign (go, menuCommand.context as GameObject);
		Undo.RegisterCreatedObjectUndo (go, "Create " + go.name);
		Selection.activeObject = go;
	}
}
