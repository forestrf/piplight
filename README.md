# README #

### How do I set this up? ###

Move the files into your project.
Attach a PipLightRenderer to a camera that needs to render lights (main camera, mostly). Ensure the Sphere mesh field is set to a sphere.
Attach the PipLight script to a gameobject that needs to act as a point light

Camera should be in HDR, linear & deferred shading mode.

### How could this be useful? ###

If you want to do something more custom with shadows, you could build upon this. Its also useful if you have need some cheap way to 'bake' shadowmaps to reuse them over frames.
It also has a level of detail in the shadowmap resolution, I'm not sure if the builtin lights has this.

### What are the tradeoffs compared to builtin lights? ###

+A LOT (10x+) quicker when shadow updates are turned off.

+Customizable, source code all here.

-Uses a lot more VRAM. Builtin lights have a fixed usage, 1 shadowmap. This uses 1 map per light, even if updating every frame.

-Slower when updating the shadowmap

-No lightmap integration at all (didn't really test)

### Are there any bugs? ###

Probably. Post them in the related unity thread, or on the issue list here.

### Can I use this on mobile? ###

Probably not. It's deferred, HDR and linear. However, you may get it to work.

### Misc ###

It re-uses shader keywords (POINT, POINT_COOKIE, SHADOWS_CUBE, SHADOWS_SOFT) already defined by unity.
Shader includes PIP_RANGE_DISCARD and PIP_SHADOW_DISCARD for early outs that seem to improve performance to me. Can be disabled by not defining them.

Some things to do:

A) Make the depth logarithmic, should improve precision / lower bias needs

B) Add proper soft shadow mode

C) Allow updating only some sides of the cubemap instead of everything

D) Remove the reference to the default sphere

E) Add an API that allows one to specify which meshes cast shadows, store them in a quad- or octree. Would allow for skipping camera culling, and to have dynamic lights which re-use shadowmaps. 
And probably better performance. And could be used with the partial cubemap updating.

F) Fix non-HDR

G) Fix gamma mode

H) Integrate cullinggroup API